name = "GroupeDeRessources_HB_XPW_Prod"
location = "southeastasia"
owner = "HB-XPW"
name_vnet = "vnet_Prod"
adress_space = [ "10.0.0.0/16" ]
name_subnet_Prod = "subnet_Prod"
name_subnet_BD-Prod = "subnet_BD-Prod"
address_prefix = "10.0.2.0/24"
alloc_method = "Static"
vm_size = "Standard_B2s"